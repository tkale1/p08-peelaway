//
//  Nodes.swift
//  PeelAway
//
//  Created by Tanmay Kale on 4/21/16.
//  Copyright © 2016 tkale1. All rights reserved.
//

import SpriteKit

class Projectile: SKShapeNode {
    convenience init(path: UIBezierPath, color: UIColor, borderColor:UIColor) {
        self.init()
        self.path = path.CGPath
        self.fillColor = color
        self.strokeColor=borderColor
    }
}

class Box: SKSpriteNode {
    var integrity: Int = 2 {
        didSet {
        if integrity > 2 {
            integrity = 2
        }
        if integrity < 0 {
            removeFromParent()
        }
        texture = SKTexture(imageNamed: "box_\(integrity)")
        }
    }
}

class Banana: SKSpriteNode {
    
        var integrity: Int = 1 {
            
            didSet {
                texture = SKTexture(imageNamed: "banana")
            }
    }
}

class Monkey: SKSpriteNode {
    var integrity: Int = 1 {
        didSet {
            texture = SKTexture(imageNamed: "monkey")
        }
    }
}

class Gorilla: SKSpriteNode {
    var integrity: Int = 1 {
        didSet {
            texture = SKTexture(imageNamed: "gorilla")
        }
    }
}

class Chimpanzee: SKSpriteNode {
    var integrity: Int = 1 {
        didSet {
            texture = SKTexture(imageNamed: "chimp")
        }
    }
}

//class scoreLabel: SKLabelNode{
//
//var integrity
//didSet {
//    scoreLabel.text = "Score:"
//}
//}