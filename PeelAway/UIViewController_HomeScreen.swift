//
//  GameViewController.swift
//  PeelAway
//
//  Created by Tanmay Kale on 4/21/16.
//  Copyright (c) 2016 tkale1. All rights reserved.
//

import UIKit
import SpriteKit

class GameViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let jeremyGif = UIImage.gifWithName("monkey1")
        let imageView = UIImageView(image: jeremyGif)
        imageView.frame = CGRect(x: 0.0, y: 20.0, width: 350.0, height: 202.0)
        

        
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return [.LandscapeRight, .LandscapeLeft]
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
}
