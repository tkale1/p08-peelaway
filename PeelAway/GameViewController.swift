//
//  GameViewController.swift
//  PeelAway
//
//  Created by Tanmay Kale on 4/21/16.
//  Copyright (c) 2016 tkale1. All rights reserved.
//

import UIKit
import SpriteKit

class GameViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let skView = view as! SKView
        //skView.showsPhysics = true
        let scene = GameScene(size:view.bounds.size)
        scene.scaleMode = .AspectFill
        
        skView.presentScene(scene)
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return [.LandscapeRight, .LandscapeLeft]
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
}
