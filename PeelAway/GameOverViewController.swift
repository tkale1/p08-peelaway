//
//  GameOverViewController.swift
//  PeelAway
//
//  Created by Pranav Bhandari on 4/24/16.
//  Copyright © 2016 tkale1. All rights reserved.
//

import UIKit
import SpriteKit

class GameOverViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        let filePath = NSBundle.mainBundle().pathForResource("gorilla_face",ofType: "gif")
        let gif = NSData(contentsOfFile: filePath!)
        
       let webViewBG = UIWebView(frame: self.view.frame)
        //let webViewBG = UIWebView(frame: CGRect(x: 0,y: 0,width: screenWidth,height: screenHeight))

        webViewBG.loadData(gif!, MIMEType: "image/gif", textEncodingName: String(), baseURL: NSURL())
        webViewBG.userInteractionEnabled = false;
        self.view.addSubview(webViewBG)
        
        let filter = UIView()
        filter.frame = self.view.frame
        filter.backgroundColor = UIColor.blackColor()
        filter.alpha = 0.05
        self.view.addSubview(filter)
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return [.LandscapeRight, .LandscapeLeft]
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
}