//
//  GameScene.swift
//  PeelAway
//
//  Created by Tanmay Kale on 4/21/16.
//  Copyright (c) 2016 tkale1. All rights reserved.
//

import SpriteKit


struct Settings {
    struct Metrics {
        static let projectileRadius = CGFloat(10)
        static let projectileRestPosition = CGPoint(x: 100, y: 100)
        static let projectileTouchThreshold = CGFloat(10)
        static let projectileSnapLimit = CGFloat(10)
        static let forceMultiplier = CGFloat(0.5)
        static let rLimit = CGFloat(50)
    }
    struct Game {
       // static let gravity = CGVector(dx: 0,dy: -9.8)
         static let gravity = CGVector(dx: 0,dy: 0)
    }
}

let gorilla = Gorilla(imageNamed: "gorilla")
let monkey = Monkey(imageNamed: "monkey")
let chimpanzee = Chimpanzee(imageNamed: "monkey")
//var projectile: Banana!
var projectile = Banana(imageNamed: "banana")
let π = CGFloat(M_PI)
var timer = NSTimer()
var rot_timer = NSTimer()
let screenSize: CGRect = UIScreen.mainScreen().bounds
let screenWidth = screenSize.width
let screenHeight = screenSize.height
var counter = 0
let monkey_lbl = SKLabelNode()
let monkey_meter = SKLabelNode()
let gorilla_lbl = SKLabelNode()
let gorilla_meter = SKLabelNode()
let chimpanzee_lbl = SKLabelNode()
let chimpanzee_meter = SKLabelNode()
var hunger_timer = NSTimer(),gorilla_timer = NSTimer(),monkey_timer = NSTimer(),monkey_hunger = NSTimer(),gorilla_hunger = NSTimer(), chimpanzee_hunger = NSTimer()
var monkey_healthTimer = NSTimer(),monkey_healthBool = true,chimpanzee_healthTimer = NSTimer(),gorilla_healthTimer = NSTimer()
var flag = false,gorilla_flag = true,monkey_flag = true,chimpanzee_flag = true
let losingSoundAction = SKAction.playSoundFileNamed("eating.wav", waitForCompletion: true)
let slingSound = SKAction.playSoundFileNamed("whip.mp3", waitForCompletion: true)
let smashSound = SKAction.playSoundFileNamed("smash.mp3", waitForCompletion: true)

var healthPoint : CGFloat = 50
var healthBar = SKSpriteNode()
var monkey_healthPoint : CGFloat = 50
var monkey_healthbar = SKSpriteNode()
var gorilla_healthPoint : CGFloat = 50
var gorilla_healthbar = SKSpriteNode()
let monkey_face = SKSpriteNode(imageNamed: "monkey_face")
let chimpanzee_face = SKSpriteNode(imageNamed: "gorillaface")
let gorilla_face = SKSpriteNode(imageNamed: "chimpanzee_face1")
var score = SKLabelNode()
var blinkStatus = false,blinkStatus2 = false,blinkStatus3 = false
var monkey_hunger2 = NSTimer(), monkey_hunger3 = NSTimer()


extension SKAction {
    
    // amplitude  - the amount the height will vary by, set this to 200 in your case.
    // timePeriod - the time it takes for one complete cycle
    // midPoint   - the point around which the oscillation occurs.
    
    static func oscillation(amplitude a: CGFloat, timePeriod t: Double, midPoint: CGPoint) -> SKAction {
        let action = SKAction.customActionWithDuration(t) { node, currentTime in
            let displacement = a * sin(2 * π * currentTime / CGFloat(t))
            node.position.y = midPoint.y + displacement
        }
        
        return action
    }
    
    
    static func oscillation1(amplitude a: CGFloat, timePeriod t: Double, midPoint: CGPoint) -> SKAction {
        let action = SKAction.customActionWithDuration(t) { node, currentTime in
            let displacement = a * sin(2 * π * currentTime / CGFloat(t))
            node.position.x = midPoint.x + displacement
        }
        
        return action
    }
}

class GameScene: SKScene {
    // var projectile: Projectile!
    var projectileIsDragged = false
    var touchCurrentPoint: CGPoint!
    var touchStartingPoint :CGPoint!
    
     func newView(view: SKView) {
        
    }
    
    
    override func didMoveToView(view: SKView) {
        //backgroundColor = UIColor.whiteColor()
        
        //stopping the music
        UIViewControllerHomeScreen.sharedHelper.audioPlayer?.stop()
        
       let background = SKSpriteNode(imageNamed: "bg.jpg")
        background.size.width = screenWidth + 100
        background.size.height = screenHeight + 100
        background.zPosition = -1
        background.position = CGPointMake(frame.size.width / 2, frame.size.height / 2)
        
        addChild(background)
    
        
        //adding monkey face
        monkey_face.position = CGPoint(x: 30, y: screenHeight - 140)
        addChild(monkey_face)
        
        //adding chimpanzee face
        chimpanzee_face.position = CGPoint(x: 30, y: screenHeight - 80)
        addChild(chimpanzee_face)
        
        //adding gorilla face
        gorilla_face.position = CGPoint(x: 30, y: screenHeight - 30)
        addChild(gorilla_face)
        
        physicsBody = SKPhysicsBody(edgeLoopFromRect: frame)
        physicsWorld.gravity = Settings.Game.gravity
        physicsWorld.speed = 0.5
        
        //setting chimpanzee healthbar
         healthPoint = 20
        healthBar.position = CGPoint(x: 85, y: screenHeight - 30)
        addChild(healthBar)
        
        //setting monkey health bar
        monkey_healthPoint = 10
        monkey_healthbar.position = CGPoint(x: 85, y: screenHeight-130)
        addChild(monkey_healthbar)
        
        //setting gorilla healthbar
        gorilla_healthPoint = 15
        gorilla_healthbar.position = CGPoint(x: 85, y: screenHeight - 80)
        addChild(gorilla_healthbar)
       
        //setting scores
        let scoreLabel = SKLabelNode(fontNamed: "Chalkduster")
        scoreLabel.text = "Score:"
        scoreLabel.horizontalAlignmentMode = .Right
        scoreLabel.position = CGPoint(x: 85, y: screenHeight - 180)
        scoreLabel.fontSize = 16
        addChild(scoreLabel)
        
        score = SKLabelNode(fontNamed: "Chalkduster")
        score.text = "0"
        score.position = CGPoint(x: 105, y: screenHeight - 180)
        score.fontSize = 16
        addChild(score)
        
        //setting collisions
        timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: "update", userInfo: nil, repeats: true)
        
        //checking for rotting
        rot_timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: "rotten", userInfo: nil, repeats: true)
        
        
        //enabling hunger timer/counter
        hunger_timer = NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: "hunger", userInfo: nil, repeats: true)
        
        //timer for chimpanzee
        chimpanzee_hunger = NSTimer.scheduledTimerWithTimeInterval(6, target: self, selector: "health_chimpanzee", userInfo: nil, repeats: true)
        
        //timer for gorilla
        gorilla_timer = NSTimer.scheduledTimerWithTimeInterval(4, target: self, selector: "move_gorilla", userInfo: nil, repeats: true)
        gorilla_hunger = NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: "health_gorilla", userInfo: nil, repeats: true)
        
        //timer for monkey
        monkey_timer = NSTimer.scheduledTimerWithTimeInterval(6, target: self, selector: "move_monkey", userInfo: nil, repeats: true)
        monkey_hunger = NSTimer.scheduledTimerWithTimeInterval(4, target: self, selector: "health_monkey", userInfo: nil, repeats: true)
        
        setupSlingshot()
        setupBoxes()
    }
    func startagain() {
        timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: "update", userInfo: nil, repeats: true)
        let triggerTime = (Int64(NSEC_PER_SEC) * 1)
        
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, triggerTime), dispatch_get_main_queue(), { () -> Void in
                      projectile.integrity = 2
            projectile.position = Settings.Metrics.projectileRestPosition
            projectile.physicsBody = SKPhysicsBody(rectangleOfSize: projectile.size)
            projectile.hidden = false
        })
    }
    
    func rotten() {
        counter += 1
        if(healthPoint > 50)
        {
            chimpanzee.runAction(SKAction .moveTo(CGPoint(x: Int(chimpanzee.position.x) - 10, y:  Int(screenHeight) - 50), duration: 2))
        }
    }
    
    func move_gorilla() {
        if(gorilla_healthPoint > 50)
        {
            gorilla.runAction(SKAction .moveTo(CGPoint(x: Int(gorilla.position.x) - 10, y:  Int(gorilla.position.y)), duration: 4))
        }
    }
    
    func move_monkey() {
        if(monkey_healthPoint > 50)
        {
            monkey.runAction(SKAction .moveTo(CGPoint(x: Int(monkey.position.x) - 10, y:  Int(monkey.position.y)), duration: 6))
        }
    }
    
    func health_monkey() {
        //monkey's health
        monkey_healthPoint = monkey_healthPoint + 5
        monkey_drawHealthBar()
    }
    func health_gorilla() {
        //gorilla's health
        gorilla_healthPoint = gorilla_healthPoint + 6
        gorilla_drawHealthBar()
    }
    func health_chimpanzee() {
        //chimpanzee's health
        healthPoint = healthPoint + 7
        drawHealthBar()
    }
    
    
    func monkey_bool() {
        monkey_healthBool = true
        monkey_timer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: "move_monkey", userInfo: nil, repeats: true)
    }
    
    func chimpanzee_bool() {
        rot_timer = NSTimer.scheduledTimerWithTimeInterval(6, target: self, selector: "rotten", userInfo: nil, repeats: true)
    }
    
    func gorilla_bool() {
        gorilla_timer = NSTimer.scheduledTimerWithTimeInterval(4, target: self, selector: "move_gorilla", userInfo: nil, repeats: true)
    }
    
    func hunger() {
     
    }
    
    func monkey_blink() {
       // monkey_healthbar.hidden = true
        if(blinkStatus)
        {
            monkey_healthbar.hidden = true
            blinkStatus = false

        }
        else {
             monkey_healthbar.hidden = false
            blinkStatus = true
        }
    }
    
    func monkey_blink2() {
        // monkey_healthbar.hidden = true
        if(blinkStatus2)
        {
            gorilla_healthbar.hidden = true
            blinkStatus2 = false
            
        }
        else {
            gorilla_healthbar.hidden = false
            blinkStatus2 = true
        }
    }
    
    func monkey_blink3() {
        // monkey_healthbar.hidden = true
        if(blinkStatus3)
        {
            healthBar.hidden = true
            blinkStatus3 = false
            
        }
        else {
            healthBar.hidden = false
            blinkStatus3 = true
        }
    }
    
    //for monkey
    func monkey_drawHealthBar() {
        
        let hb_x1 = monkey_healthbar.position.x
        let hb_y1 = monkey_healthbar.position.y
        self.removeChildrenInArray([monkey_healthbar])
        let healthBarWidth = self.size.width/15
        let healthBarHeight = self.size.height / 25
        
        let widthOfHealth = (self.size.width/15 - 2.0) * monkey_healthPoint / 100
        let clearColor = UIColor.clearColor()
        var fillColor = UIColor()
        var monkey_hunger1 = NSTimer()
        if(monkey_healthPoint > 75)
        {
            fillColor = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1.0)
            monkey_hunger1 = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "monkey_blink", userInfo: nil, repeats: true)
        }
        else
        {
            fillColor = UIColor(red: 113/255, green: 202/255, blue: 53/255, alpha: 1.0)
            monkey_hunger1.invalidate()
        }
        
        let borderColor = UIColor(red: 35/255, green: 28/255, blue: 40/255, alpha: 1.0)
        
        //outline
        let outlineRectSize = CGSizeMake(healthBarWidth - 1, healthBarHeight - 1)
        UIGraphicsBeginImageContextWithOptions(outlineRectSize, false, 0)
        let healthBarContext = UIGraphicsGetCurrentContext()
        
        //drawing outline
        let spriteOutlineRect = CGRectMake(0, 0, healthBarWidth - 1, healthBarHeight - 1)
        CGContextSetStrokeColorWithColor(healthBarContext, borderColor.CGColor)
        CGContextSetLineWidth(healthBarContext, 1)
        CGContextAddRect(healthBarContext, spriteOutlineRect)
        CGContextStrokePath(healthBarContext)
        
        //fill health bar
        var spriteFillRect = CGRectMake(0.5, 0.5, outlineRectSize.width - 1, outlineRectSize.height - 1)
        spriteFillRect.size.width = widthOfHealth
        CGContextSetFillColorWithColor(healthBarContext, fillColor.CGColor)
        CGContextSetStrokeColorWithColor(healthBarContext, clearColor.CGColor)
        CGContextSetLineWidth(healthBarContext, 1)
        CGContextFillRect(healthBarContext, spriteFillRect)
        
        //generate a spite image of 2 pieces for display
        let spriteImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let spriteCGImageRef = spriteImg.CGImage
        let spriteTexture = SKTexture(CGImage: spriteCGImageRef!)
        spriteTexture.filteringMode = SKTextureFilteringMode.Linear
        monkey_healthbar = SKSpriteNode(texture: spriteTexture, size: outlineRectSize)
        monkey_healthbar.position = CGPoint(x: hb_x1, y: hb_y1)
        
        monkey_healthbar.name = "MonkeyHealthBar"
        monkey_healthbar.zPosition = 2
        self.addChild(monkey_healthbar)
    }
    
    
    //for gorilla
    func gorilla_drawHealthBar() {
        
        let hb_x1 = gorilla_healthbar.position.x
        let hb_y1 = gorilla_healthbar.position.y
        self.removeChildrenInArray([gorilla_healthbar])
        let healthBarWidth = self.size.width/15
        let healthBarHeight = self.size.height / 25
        
        let widthOfHealth = (self.size.width/15 - 2.0) * gorilla_healthPoint / 100
        let clearColor = UIColor.clearColor()
        var fillColor = UIColor()
        
        if(gorilla_healthPoint > 75)
        {
            fillColor = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1.0)
            monkey_hunger2 = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "monkey_blink2", userInfo: nil, repeats: true)
        }
        else
        {
            fillColor = UIColor(red: 113/255, green: 202/255, blue: 53/255, alpha: 1.0)
            monkey_hunger2.invalidate()
        }
            let borderColor = UIColor(red: 35/255, green: 28/255, blue: 40/255, alpha: 1.0)
        
        //outline
        let outlineRectSize = CGSizeMake(healthBarWidth - 1, healthBarHeight - 1)
        UIGraphicsBeginImageContextWithOptions(outlineRectSize, false, 0)
        let healthBarContext = UIGraphicsGetCurrentContext()
        
        //drawing outline
        let spriteOutlineRect = CGRectMake(0, 0, healthBarWidth - 1, healthBarHeight - 1)
        CGContextSetStrokeColorWithColor(healthBarContext, borderColor.CGColor)
        CGContextSetLineWidth(healthBarContext, 1)
        CGContextAddRect(healthBarContext, spriteOutlineRect)
        CGContextStrokePath(healthBarContext)
        
        //fill health bar
        var spriteFillRect = CGRectMake(0.5, 0.5, outlineRectSize.width - 1, outlineRectSize.height - 1)
        spriteFillRect.size.width = widthOfHealth
        CGContextSetFillColorWithColor(healthBarContext, fillColor.CGColor)
        CGContextSetStrokeColorWithColor(healthBarContext, clearColor.CGColor)
        CGContextSetLineWidth(healthBarContext, 1)
        CGContextFillRect(healthBarContext, spriteFillRect)
        
        //generate a spite image of 2 pieces for display
        let spriteImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let spriteCGImageRef = spriteImg.CGImage
        let spriteTexture = SKTexture(CGImage: spriteCGImageRef!)
        spriteTexture.filteringMode = SKTextureFilteringMode.Linear
        gorilla_healthbar = SKSpriteNode(texture: spriteTexture, size: outlineRectSize)
        
        gorilla_healthbar.position = CGPoint(x: hb_x1, y: hb_y1)
        gorilla_healthbar.name = "GorillaHealthBar"
        gorilla_healthbar.zPosition = 2
        self.addChild(gorilla_healthbar)
    }

    
    //for chimpanzee
    func drawHealthBar() {
        
        let hb_x = healthBar.position.x
        let hb_y = healthBar.position.y
        self.removeChildrenInArray([healthBar])
        let healthBarWidth = self.size.width/15
        let healthBarHeight = self.size.height / 25
        
        let widthOfHealth = (self.size.width/15 - 2.0) * healthPoint / 100
        let clearColor = UIColor.clearColor()
        var fillColor = UIColor()
        if(healthPoint > 75)
        {
            fillColor = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1.0)
            monkey_hunger3 = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "monkey_blink3", userInfo: nil, repeats: true)
        }
        else
        {
            fillColor = UIColor(red: 113/255, green: 202/255, blue: 53/255, alpha: 1.0)
            monkey_hunger3.invalidate()
        }
        let borderColor = UIColor(red: 35/255, green: 28/255, blue: 40/255, alpha: 1.0)
        
        //outline
        let outlineRectSize = CGSizeMake(healthBarWidth - 1, healthBarHeight - 1)
        UIGraphicsBeginImageContextWithOptions(outlineRectSize, false, 0)
        let healthBarContext = UIGraphicsGetCurrentContext()
        
        //drawing outline
        let spriteOutlineRect = CGRectMake(0, 0, healthBarWidth - 1, healthBarHeight - 1)
        CGContextSetStrokeColorWithColor(healthBarContext, borderColor.CGColor)
        CGContextSetLineWidth(healthBarContext, 1)
        CGContextAddRect(healthBarContext, spriteOutlineRect)
        CGContextStrokePath(healthBarContext)
        
        //fill health bar
        var spriteFillRect = CGRectMake(0.5, 0.5, outlineRectSize.width - 1, outlineRectSize.height - 1)
        spriteFillRect.size.width = widthOfHealth
        CGContextSetFillColorWithColor(healthBarContext, fillColor.CGColor)
        CGContextSetStrokeColorWithColor(healthBarContext, clearColor.CGColor)
        CGContextSetLineWidth(healthBarContext, 1)
        CGContextFillRect(healthBarContext, spriteFillRect)
        
        //generate a spite image of 2 pieces for display
        let spriteImg = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let spriteCGImageRef = spriteImg.CGImage
        let spriteTexture = SKTexture(CGImage: spriteCGImageRef!)
        spriteTexture.filteringMode = SKTextureFilteringMode.Linear
        healthBar = SKSpriteNode(texture: spriteTexture, size: outlineRectSize)
        healthBar.position = CGPoint(x: hb_x, y: hb_y)
        healthBar.name = "enemyHealthBar"
        healthBar.zPosition = 2
        self.addChild(healthBar)
    }
    
    
    
    
    // MARK: - intersection
    func update() {
        
        if(chimpanzee.position.x <= 225 || healthPoint >= 100 || monkey.position.x <= 225 || monkey_healthPoint >= 100 || gorilla.position.x <= 225 || gorilla_healthPoint >= 100)
            
        {
            
            rot_timer.invalidate()
          
          /*  if !UIAccessibilityIsReduceTransparencyEnabled() {
                self.view!.backgroundColor = UIColor.clearColor()
                
                let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
                let blurEffectView = UIVisualEffectView(effect: blurEffect)
                //always fill the view
                blurEffectView.frame = self.view!.bounds
                blurEffectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
                
                self.view!.addSubview(blurEffectView) //if you have more UIViews, use an insertSubview API to place it where needed
            } 
            else {
                self.view!.backgroundColor = UIColor.blackColor()
            }
            
*/
            
            if !UIAccessibilityIsReduceTransparencyEnabled() {
                self.view!.backgroundColor = UIColor.clearColor()
                let blurEffect: UIBlurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
                let blurEffectView: UIVisualEffectView = UIVisualEffectView(effect: blurEffect)
                blurEffectView.frame = self.view!.bounds
                blurEffectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
                self.view!.addSubview(blurEffectView)
                
                
                
                let vibrancyEffect: UIVibrancyEffect = UIVibrancyEffect(forBlurEffect: blurEffect)
                let vibrancyEffectView: UIVisualEffectView = UIVisualEffectView(effect: vibrancyEffect)
                vibrancyEffectView.frame = self.view!.bounds
                // Label for vibrant text
                let vibrantLabel: UILabel = UILabel()
                var vibrantButton: UIButton = UIButton()
                
                vibrantLabel.text = "Congratulations, You feeded yourself to the Monkeys. Your Score :" + score.text!
                vibrantLabel.font = UIFont.systemFontOfSize(20.0)
                vibrantLabel.sizeToFit()
                vibrantLabel.center = self.view!.center
                
                // Add label to the vibrancy view
                vibrancyEffectView.contentView.addSubview(vibrantLabel)
                
                // Add the vibrancy view to the blur view
                blurEffectView.contentView.addSubview(vibrancyEffectView)

                
            }
            else
            {
              self.view!.backgroundColor = UIColor.blackColor()
            }
          
            
            
            
        }
        
        if(monkey.position.x <= 225 || monkey_healthPoint >= 100)
        {
            monkey_timer.invalidate()
        }
        
        if(gorilla.position.x <= 225 || gorilla_healthPoint >= 100)
        {
            gorilla_timer.invalidate()
        }
        
        //checking for rotting
        if(counter > 4)
        {
           // sayings.text = "Banana has roten"
            
             //[self runAction:[SKAction moveTo:newPosiion duration:0.0]];
           // monkey.runAction(SKAction .moveTo(CGPoint(x: 200, y:  Int(screenHeight/3) - 50), duration: 1))
        }
        // Checking for intersection
        if(CGRectIntersectsRect(gorilla.frame, projectile.frame))
        {
            //removing gravity
            physicsWorld.gravity = CGVector(dx: 0,dy: 0)
           projectile.hidden = true
           timer.invalidate()
            if(flag)
            {
                flag = false
                //progressNode.size.width = CGFloat(progressNode.size.width) * 2
                gorilla_healthPoint = gorilla_healthPoint - 4
                gorilla_drawHealthBar()
                self.runAction(losingSoundAction)
                gorilla_timer.invalidate()
                gorilla_healthTimer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: "gorilla_bool", userInfo: nil, repeats: false)
                var num:Int = Int(score.text!)!
                num += 15
                score.text = String(num)
            }
           startagain()
        }
        
        
        //checking for monkey intersection
        if(CGRectIntersectsRect(monkey.frame, projectile.frame))
        {
            //removing gravity
            physicsWorld.gravity = CGVector(dx: 0,dy: 0)
            projectile.hidden = true
            timer.invalidate()
            if(flag)
            {
                flag = false
                monkey_healthPoint = monkey_healthPoint - 3
                monkey_drawHealthBar()
                self.runAction(losingSoundAction)
                monkey_timer.invalidate()
                monkey_healthTimer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: "monkey_bool", userInfo: nil, repeats: false)
                var num:Int = Int(score.text!)!
                num += 20
                score.text = String(num)
            }
            
            startagain()
        }
        
        //checking for chimpanzee intersection
        if(CGRectIntersectsRect(chimpanzee.frame, projectile.frame))
        {
            //removing gravity
            physicsWorld.gravity = CGVector(dx: 0,dy: 0)
            projectile.hidden = true
            timer.invalidate()
            if(flag)
            {
                flag = false
                healthPoint = healthPoint - 5
                drawHealthBar()
                self.runAction(losingSoundAction)
                rot_timer.invalidate()
                chimpanzee_healthTimer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: "chimpanzee_bool", userInfo: nil, repeats: false)
                var num:Int = Int(score.text!)!
                num += 10
                score.text = String(num)
            }
            startagain()
        }
        
        //check if it crossed the screen
        if(projectile.position.x > screenWidth - 30)
        {
            //removing gravity
            physicsWorld.gravity = CGVector(dx: 0,dy: 0)
            projectile.hidden = true
            timer.invalidate()
            if(flag)
            {
                flag = false
                var num:Int = Int(score.text!)!
                if(num > 9)
                {
                    num -= 10
                }
                score.text = String(num)
                self.runAction(smashSound)

            }
            startagain()
            
        }
    }
 
    //fuction to setting up the sling
    func setupSlingshot() {
       
        projectile.integrity = 2
        projectile.position = Settings.Metrics.projectileRestPosition
       projectile.size.width = 30
        projectile.size.height = 30
        projectile.physicsBody = SKPhysicsBody(rectangleOfSize: projectile.size)
        
        addChild(projectile)
       // addChild(projectile)
    
        //calling healthbar functions
        self.drawHealthBar()
        monkey_drawHealthBar()
        gorilla_drawHealthBar()
        
     }
    
    
    //function to setup monkeys
    
    func setupBoxes() {
        
       
        monkey.integrity = 2
        monkey.position = CGPoint(x: Int(screenWidth) - 100, y:  Int(screenHeight/3) - 50)
        //monkey.physicsBody = SKPhysicsBody(rectangleOfSize: monkey.size)
        monkey.physicsBody?.dynamic = true
        addChild(monkey)
        
        let oscillate1 = SKAction.oscillation(amplitude: 10,
                                             timePeriod: 1,
                                             midPoint: monkey.position)
        monkey.runAction(SKAction.repeatActionForever(oscillate1))
        
        
       
        gorilla.integrity = 2
        gorilla.position = CGPoint(x: Int(screenWidth) - 100, y:  2*Int(screenHeight/3) - 50)
        // gorilla.physicsBody = SKPhysicsBody(rectangleOfSize: monkey.size)
        addChild(gorilla)
        
        let oscillate = SKAction.oscillation1(amplitude: 10,
            timePeriod: 1,
            midPoint: gorilla.position)
        gorilla.runAction(SKAction.repeatActionForever(oscillate))
    
        //adding chimpanzee
        chimpanzee.integrity = 2
        chimpanzee.position = CGPoint(x: Int(screenWidth) - 100, y:  Int(screenHeight) - 50)
        addChild(chimpanzee)
        let oscillate2 = SKAction.oscillation(amplitude: 10,
                                             timePeriod: 1,
                                             midPoint: chimpanzee.position)
        chimpanzee.runAction(SKAction.repeatActionForever(oscillate2))
        
        

        
}
    
    func fingerDistanceFromProjectileRestPosition(projectileRestPosition: CGPoint, fingerPosition: CGPoint) -> CGFloat {
        return sqrt(pow(projectileRestPosition.x - fingerPosition.x,2) + pow(projectileRestPosition.y - fingerPosition.y,2))
    }
    
    func projectilePositionForFingerPosition(fingerPosition: CGPoint, projectileRestPosition:CGPoint, circleRadius rLimit:CGFloat) -> CGPoint {
        let φ = atan2(fingerPosition.x - projectileRestPosition.x, fingerPosition.y - projectileRestPosition.y)
        let cX = sin(φ) * rLimit
        let cY = cos(φ) * rLimit
        return CGPoint(x: cX + projectileRestPosition.x, y: cY + projectileRestPosition.y)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        func shouldStartDragging(touchLocation:CGPoint, threshold: CGFloat) -> Bool {
            let distance = fingerDistanceFromProjectileRestPosition(
                Settings.Metrics.projectileRestPosition,
                fingerPosition: touchLocation
            )
            return distance < Settings.Metrics.projectileRadius + threshold
        }
        
        if let touch = touches.first {
            let touchLocation = touch.locationInNode(self)
            
            if !projectileIsDragged && shouldStartDragging(touchLocation, threshold: Settings.Metrics.projectileTouchThreshold)  {
                touchStartingPoint = touchLocation
                touchCurrentPoint = touchLocation
                projectileIsDragged = true
            }
        }
        flag = true
        
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if projectileIsDragged {
            if let touch = touches.first {
                let touchLocation = touch.locationInNode(self)
                let distance = fingerDistanceFromProjectileRestPosition(touchLocation, fingerPosition: touchStartingPoint)
                if distance < Settings.Metrics.rLimit  {
                    touchCurrentPoint = touchLocation
                } else {
                    touchCurrentPoint = projectilePositionForFingerPosition(
                        touchLocation,
                        projectileRestPosition: touchStartingPoint,
                        circleRadius: Settings.Metrics.rLimit
                    )
                }
                
            }
            projectile.position = touchCurrentPoint
         
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if projectileIsDragged {
            projectileIsDragged = false
            let distance = fingerDistanceFromProjectileRestPosition(touchCurrentPoint, fingerPosition: touchStartingPoint)
            if distance > Settings.Metrics.projectileSnapLimit {
                let vectorX = touchStartingPoint.x - touchCurrentPoint.x
                let vectorY = touchStartingPoint.y - touchCurrentPoint.y
                projectile.physicsBody = SKPhysicsBody(circleOfRadius: Settings.Metrics.projectileRadius)
                projectile.physicsBody?.applyImpulse(
                    CGVector(
                        dx: vectorX * Settings.Metrics.forceMultiplier,
                        dy: vectorY * Settings.Metrics.forceMultiplier
                    )
                )
            } else {
                projectile.physicsBody = nil
                projectile.position = Settings.Metrics.projectileRestPosition
            }
        }
        physicsWorld.gravity = CGVector(dx: 0,dy: -9.8)
        counter = 0
        self.runAction(slingSound)
    }
    
}

