//
//  UIViewControllerHomeScreen.swift
//  PeelAway
//
//  Created by Tanmay Kale on 4/24/16.
//  Copyright © 2016 tkale1. All rights reserved.
//

import UIKit
import SpriteKit
import AVFoundation


class UIViewControllerHomeScreen: UIViewController {
    static let sharedHelper = UIViewControllerHomeScreen()
    var audioPlayer: AVAudioPlayer?
    func playBackgroundMusic() {
        let aSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("junglestartsound", ofType: "wav")!)
        do {
            audioPlayer = try AVAudioPlayer(contentsOfURL:aSound)
            audioPlayer!.numberOfLoops = -1
            audioPlayer!.prepareToPlay()
            audioPlayer!.play()
        } catch {
            print("Cannot play the file")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       UIViewControllerHomeScreen.sharedHelper.playBackgroundMusic()
        
        
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        let background = UIImage(named: "background2.jpg")
        var back : UIImageView!
        back = UIImageView(frame: view.bounds)
        back.contentMode =  UIViewContentMode.ScaleAspectFill
        back.clipsToBounds = true
        back.image = background
        back.center = view.center
        view.addSubview(back)
        self.view.sendSubviewToBack(back)
        
        let monkey1 = UIImage.gifWithName("monkey1")
        let imageView = UIImageView(image: monkey1)
        imageView.frame = CGRect(x: 60, y: 10 , width: 180, height: 202.0)
        view.addSubview(imageView)

        let imageView1 = UIImageView(image: monkey1)
        imageView1.frame = CGRect(x: screenWidth-200, y: 20.0, width: 180, height: 202.0)
        view.addSubview(imageView1)

        let monkey2 = UIImage.gifWithName("monkey2")
        let imageView2 = UIImageView(image: monkey2)
        imageView2.frame = CGRect(x:170, y: 100, width: 120, height: 40)
        view.addSubview(imageView2)
    
        let monkey3 = UIImage.gifWithName("monkey3left")
        let imageView3 = UIImageView(image: monkey3)
        imageView3.frame = CGRect(x:60, y: screenHeight/2-20, width: 50, height: 60)
        view.addSubview(imageView3)
        
        let monkey4 = UIImage.gifWithName("monkey3right")
        let imageView4 = UIImageView(image: monkey4)
        imageView4.frame = CGRect(x:screenWidth-60, y: screenHeight/2+50, width: 50, height: 60)
        view.addSubview(imageView4)
        
        let imageView5 = UIImageView(image: monkey2)
        imageView5.frame = CGRect(x: screenWidth-120, y: 100, width: 120, height: 40)
        view.addSubview(imageView5)
        view.bringSubviewToFront(imageView1)
        
        let boardname = "board"
        let boardimg = UIImage(named: boardname)
        let boardsign = UIImageView(image: boardimg!)
        boardsign.frame = CGRect(x:screenWidth/3+30, y: screenHeight/4, width: 150, height: 270)
        view.addSubview(boardsign)

    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return [.LandscapeRight, .LandscapeLeft]
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }

}
